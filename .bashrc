#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='\e[1;32m[\u@\h \w]\$ \e[0m'

alias config="/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME"
