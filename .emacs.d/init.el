;;; init.el -- my emacs init file
;;; Commentary:

;;; Code:
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(setq inhibit-startup-screen t)
(setq initial-scratch-message nil)

(set-frame-font "Source Code Pro 10")

(setq make-backup-files nil)
(setq auto-save-default nil)

(setq scroll-step 1)
(setq scroll-conservatively 10000)

(global-display-line-numbers-mode)
(global-hl-line-mode)
(setq-default display-line-numbers-width 4)

(setq-default cursor-type 'bar)

(electric-pair-mode)

(windmove-default-keybindings 'control)

(ido-mode)

(use-package ido-vertical-mode
  :ensure t
  :init (ido-vertical-mode)
  :config (setq ido-vertical-define-keys 'C-n-and-C-p-only))

(use-package smex
  :ensure t
  :bind ("M-x" . smex))

(use-package company
  :ensure t
  :init (global-company-mode)
  :bind (:map company-active-map
	      ("C-n" . company-select-next-or-abort)
	      ("C-p" . company-select-previous-or-abort)))

(use-package darktooth-theme
  :ensure t
  :init (load-theme 'darktooth t))

(use-package pdf-tools
  :ensure t
  :init (pdf-tools-install)
  :config (setq-default pdf-view-display-size 'fit-page)
  :hook (pdf-view-mode . (lambda () (display-line-numbers-mode -1))))

(use-package aggressive-indent
  :ensure t
  :init (global-aggressive-indent-mode))

(setq-default elfeed-search-filter "@1-week-ago +unread ")

(use-package elfeed
  :ensure t
  :init (elfeed)
  :bind ("C-x w" . elfeed))

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(elfeed-feeds
   (quote
    ("https://congratulations.libsyn.com/rss" "http://bit.ly/2qkeaOp" "http://feeds.feedburner.com/porfalarnoutracoisa" "http://www.zerozero.pt/rss/noticias.php" "http://feeds.feedburner.com/expresso-geral" "http://feeds.feedburner.com/cidade-quem-nunca" "https://feeds.feedburner.com/Sporting160" "http://feeds.tsf.pt/Tsf-GovernoSombra-Podcast")))
 '(package-selected-packages
   (quote
    (elfeed aggressive-indent pdf-tools darktooth-theme company smex ido-vertical-mode use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(provide 'init.el)
;;; init.el ends here
